#!/usr/bin/env ruby

require 'etc'
require 'fileutils'
require 'set'

require 'ruby_parser'

#######################################################################
# Process the code
#######################################################################

lib = []
bin = []

nodes = Set.new
requires = []

process = lambda do |f, fname|
  nodes << fname
  parser = RubyParser.new
  tree = parser.parse(File.read(f))
  tree.each do |node|
    if node && node[0] == :call && node[2] == :require
      nodes << node[3][1]
      requires << [fname, node[3][1]]
    end
  end
end

Dir.chdir 'lib' do
  Dir.glob('**/*.rb').each do |f|
    fname = f.sub(/\.rb$/, '')
    process.call(f, fname)
    lib << fname
  end
end if Dir.exist?('lib')

Dir.glob('bin/*').each do |f|
  process.call(f, f)
  bin << f
end


#######################################################################
# Decide what to do with the data based in the called program name
# rrg: output to stdout
# rrgv: view image directly
#######################################################################

# TODO refactor this to polymorphism
if File.basename($PROGRAM_NAME) == 'rrg'
  output = $stdout
  view_output = lambda { true } # nothing
else

  cache_dir = File.join(Etc.getpwuid.dir, '.cache', 'rrg')
  FileUtils.mkdir_p(cache_dir)

  project_name = File.basename(Dir.pwd)
  timestamp = Time.now.to_i.to_s

  image = File.join(cache_dir, project_name + '.' + timestamp + '.png')
  output = IO.popen(['dot', '-Tpng', '-o', image], 'w')

  # FIXME hardcoded list of image viewers for now
  image_viewer = %w[xdg-open eog gwenview ristretto open].find do |prog|
    system("which #{prog} >/dev/null 2>&1")
  end

  unless image_viewer
    echo "E: could find a suitable image viewer. Please report a bug."
    exit(1)
  end

  view_output = lambda do
    system(image_viewer, image)
  end
end

#######################################################################
# Generate output
#######################################################################

output.puts 'digraph require_graph {'
nodes.each do |req|
  if lib.include?(req)
    color = ''
  elsif bin.include?(req)
    color = ', color="#729fcf", fontcolor="#729fcf"'
  else
    color = ', color="#888a85", fontcolor="#888a85"'
  end
  output.puts "  \"#{req}\" [shape=box#{color}];"
end
requires.each do |call|
  output.puts "  \"#{call[0]}\" -> \"#{call[1]}\";"
end
output.puts '}'
output.close

#######################################################################
# visualize the output
#######################################################################
view_output.call
