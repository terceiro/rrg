# Rrg

TODO: Write a gem description

## Installation

With rubygems:

    $ gem install rrg

## Usage

Just run `rrg` at the root of your project. `rrg` will parse the code inside
`lib/`, and generate a graph description in the Graphviz format. You can pipe
the output to Graphviz directly, or store it in a file and process it the
generate an image.

If you call `rrgv`, it wil automatically process the graph with Graphviz,
generate a PNG image, and open it with `xdg-open` or `open`.

## Contributing

1. Fork the [project on GitLab](https://gitlab.com/terceiro/rrg)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request
