# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rrg/version'

description = <<DESCRIPTION
rrg will read the source code of a Ruby project and generate a graph based on
the require statements in the code; nodes represent the source files and an
arrow from A to B means that A contains a `require 'B'` statement.
DESCRIPTION

Gem::Specification.new do |spec|
  spec.name          = "rrg"
  spec.version       = Rrg::VERSION
  spec.authors       = ["Antonio Terceiro"]
  spec.email         = ["terceiro@softwarelivre.org"]
  spec.summary       = %q{require graph extractor and viewer for Ruby projects}
  spec.description   = description
  spec.homepage      = "https://gitlab.com/terceiro/rrg"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake"

  spec.add_runtime_dependency "ruby_parser"
end
